import { Component, OnInit } from '@angular/core';
import { StarwarsService } from 'app/starwars.service';

@Component({
  selector: 'app-starwars',
  templateUrl: './starwars.component.html',
  styleUrls: ['./starwars.component.css']
})
export class StarwarsComponent implements OnInit {
  results: any;
  nombrePersonajes: string[] = [];
  persona: any;
  nuevaListaPeliculas: any = [];
  detalleDePeli: string[] = [];
  detalleTodo: any
  constructor(private service: StarwarsService) {

    this.service.obtenerPeliculas('http://localhost:8080/prueba/films').subscribe((res: any) => {
      this.results = res.results
    })
  }
  director: string;
  producer: string;
  episode_id: any;
  release_date: string;

  public detalle(starwars) {
    this.service.obtenerDetalle(starwars.episode_id).subscribe((res: any) => {
      
      this.director = res.director;
      this.producer = res.producer;
      this.episode_id = res.episode_id;
      this.release_date = res.release_date;
      this.detalleDePeli.push(res.opening_crawl, res.director, res.producer, res.episode_id, res.release_date);
      this.detalleTodo = this.detalleDePeli;
    })
    
    for (let x of starwars.characters) {
      let nombres: string = x;

      this.service.obtenerPersonajes(nombres).subscribe((res: any) => {

        this.nombrePersonajes.push(res);
        //this.nombrePersonajes.push(res.films);

       this.nombrePersonajes.forEach((e: any) => {
         e["nuevaListaPeliculas"] = [];
         e["films"].forEach((f) => {
          console.log(f);
           this.service.obtenerFilmxPersonajes(f).subscribe((res1:any)=>{
           e["nuevaListaPeliculas"].push(res1.title);
           console.log(res1.title);
           });
           
         })
       })
        
        this.persona = this.nombrePersonajes;
      });
    }
  }

  public detallePeli(i,personaje){
    console.log(i);
    console.log(personaje);
  }

  public borrar() {
    this.detalleDePeli = [];
    this.detalleTodo = [];
    this.nombrePersonajes = [];
  }
  ngOnInit() {

  }
}
