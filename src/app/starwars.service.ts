import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';

@Injectable()
export class StarwarsService {
  public headers = new Headers();
  public urlpersonas: string = "http://localhost:8080/prueba/films";

  constructor(private httpClient: HttpClient, public _http: Http) {
    this.headers.append("Content-Type", "application/json");
  }

  protected obtenerHeaders(): Headers {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Acces-Control-Allow-Headers', 'Content-Type');
    return headers;
  }

  obtenerPeliculas(url: string) {

    console.log(url)
    console.log(this.httpClient.get(url))
    return this.httpClient.get(url)
  }

  obtenerPersonajes(url: string) {
    let queryParams = new URLSearchParams();
    let headers = this.obtenerHeaders();
    headers.append('requestUrl', url);
    let options = new RequestOptions({
      headers: headers,
      search: queryParams
    });
    return this._http.get('http://localhost:8080/prueba/character', options).map((res: Response) => res.json());
  }

  obtenerFilmxPersonajes(url: string) {
    let queryParams = new URLSearchParams()
    let headers = this.obtenerHeaders();
    headers.append('requestUrl2', url);
    let options = new RequestOptions({
      headers: headers,
      search: queryParams
    });
    return this._http.get('http://localhost:8080/prueba/filmxcharacter', options).map((res: Response) => res.json());
  }

  obtenerDetalle(filmName: string) {
    let queryParams = new URLSearchParams()
    let headers = this.obtenerHeaders();
    headers.append('filmName', filmName);
    let options = new RequestOptions({
      headers: headers,
      search: queryParams
    })
    return this._http.get('http://localhost:8080/prueba/detail', options).map((res: Response) => res.json());
  }

}

