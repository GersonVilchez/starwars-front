import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { StarwarsComponent } from './starwars/starwars.component';
import { StarwarsService } from './starwars.service';

@NgModule({
  declarations: [
    AppComponent,
    StarwarsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'starwars', pathMatch: 'full' },
      { path: 'starwars', component: StarwarsComponent }
    ])
  ],
  providers: [StarwarsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
